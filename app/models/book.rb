class Book < ApplicationRecord

    def self.expensive
        where('price > 500000')
    end

    def self.mahal(price)
        where('price>= ?',price)
    end
    
end
