class StudentsController < ApplicationController
  def new
    @student = Student.new
  end

  def create
    student = Student.new(resource_params)
    student.save
    flash[:notice] = "Student has been created" 
    redirect_to students_path
  end

  def index
    @students = Student.all
  end

  def edit
    @student = Student.find(params[:id])
  end

  def update
    @student = Student.find(params[:id])
    @student.update(resource_params)
    flash[:notice] = "student has been Updated"
    redirect_to students_path(@student)
  end

  def show
    id = params[:id]
    @student = Student.find(id)
  end

  def destroy
    @student = Student.find(params[:id])
    @student.destroy
    flash[:notice] = "student has been Deleted"
    redirect_to students_path
  end

  private
  def resource_params
    params.require(:student).permit(:nama, :username, :age, :rombel, :address, :city, :nik)
  end

end
