class BooksController < ApplicationController
  def new #untuk menampilkan data baru
    @book = Book.new
  end

  def create #untuk memproses data baru yang dimasukan di form new
    #render plain: params.inspect -> cara mengecek apa is params
    #titlr = params[:book]:[titlr] -> cara mengecek balikan params
    book = Book.new(resource_params)
    book.save
    flash[:notice] = "book has been created" #buat ngasi pesan pas udah di create
    #puts book.errors.masages
    redirect_to books_path
  end

  def edit #menampilkan data yang sudah di simpan di edit
    @book = Book.find(params[:id])
  end

  def update #melakukan proses ketika user mengedit data
    @book = Book.find(params[:id])
    @book.update(resource_params)
    flash[:notice] = "book has been Updated"
    redirect_to books_path(@book)
  end

  def destroy #untuk menghapus data
    @book = Book.find(params[:id])
    @book.destroy
    flash[:notice] = "book has been Deleted"
    redirect_to books_path
  end

  def index #menampilkan seluruh data yang ada di database
    @books = Book.all
  end

  def show #menampilkan sebuah data secara detail
    id = params[:id]
    @book = Book.find(id)
    #render plain: id
    #render plain: @book.titlr
  end

  private
  def resource_params
    params.require(:book).permit(:titlr, :page, :price, :description)
  end

end

