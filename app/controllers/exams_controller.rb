class ExamsController < ApplicationController
    def new
      @exam = Exam.new
    end
  
    def create
      exam = Exam.new(resource_params)
      exam.save
      flash[:notice] = "exam has been created" 
      redirect_to exams_path
    end
  
    def index
      @exams = Exam.all
    end
  
    def edit
      @exam = Exam.find(params[:id])
    end
  
    def update
      @exam = Exam.find(params[:id])
      @exam.update(resource_params)
      flash[:notice] = "exam has been Updated"
      redirect_to exams_path(@exam)
    end
  
    def show
      id = params[:id]
      @exam = exam.find(id)
    end
  
    def destroy
      @exam = Exam.find(params[:id])
      @exam.destroy
      flash[:notice] = "exam has been Deleted"
      redirect_to exams_path
    end
  
    private
    def resource_params
      params.require(:exam).permit(:title, :mapel, :duration, :nilFai, :status, :level, :student_id)
    end
  end
  