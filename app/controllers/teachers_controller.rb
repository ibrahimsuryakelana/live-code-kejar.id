class TeachersController < ApplicationController
  def new
    @teacher = Teacher.new
  end

  def create
    teacher = Teacher.new(resource_params)
    teacher.save
    flash[:notice] = "teacher has been created" 
    redirect_to teachers_path
  end

  def index
    @teachers = Teacher.all
  end

  def edit
    @teacher = Teacher.find(params[:id])
  end

  def update
    @teacher = Teacher.find(params[:id])
    @teacher.update(resource_params)
    flash[:notice] = "teacher has been Updated"
    redirect_to teachers_path(@teacher)
  end

  def show
    id = params[:id]
    @teacher = Teacher.find(id)
  end

  def destroy
    @teacher = Teacher.find(params[:id])
    @teacher.destroy
    flash[:notice] = "teacher has been Deleted"
    redirect_to teachers_path
  end

  private
  def resource_params
    params.require(:teacher).permit(:nik, :nama, :age, :kelas, :mapel)
  end
end
