Rails.application.routes.draw do
  # get 'reports/index'
  # get 'reports/new'
  # get 'reports/edit'
  # get 'reports/show'
  # get 'payments/index'
  # get 'payments/new'
  # get 'payments/edit'
  # get 'payments/show'
  # get 'teachers/index'
  # get 'teachers/new'
  # get 'teachers/edit'
  # get 'teachers/show'
  # get 'exam/index'
  # get 'exam/edit'
  # get 'exam/new'
  # get 'exam/show'
  # get 'students/index'
  # get 'students/new'
  # get 'students/edit'
  # get 'students/show'
 resources :books
 resources :students
 resources :exams
 resources :teachers
 resources :payments
 resources :reports
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
