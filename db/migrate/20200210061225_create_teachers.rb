class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers do |t|
      t.integer :nik
      t.string :nama
      t.integer :age
      t.string :kelas
      t.string :mapel

      t.timestamps
    end
  end
end
