class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :titlr, default: `belum ada judul`, limit: 100
      t.integer :page, null: false 
      t.integer :price,default: 0
      t.text :description

      t.timestamps
    end
  end
  def down
    drop_table :books
end
