class CreateAuthors < ActiveRecord::Migration[6.0]
  def change
    create_table :authors do |t|
      t.string :name default:`belum ada nama`, limit:50
      t.integer :age ,null:false
      t.text :address, default:0

      t.timestamps
    end
  end
end
