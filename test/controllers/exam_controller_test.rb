require 'test_helper'

class ExamControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get exam_index_url
    assert_response :success
  end

  test "should get edit" do
    get exam_edit_url
    assert_response :success
  end

  test "should get new" do
    get exam_new_url
    assert_response :success
  end

  test "should get show" do
    get exam_show_url
    assert_response :success
  end

end
