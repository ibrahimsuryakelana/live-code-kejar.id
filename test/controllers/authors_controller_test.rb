require 'test_helper'

class AuthorsControllerTest < ActionDispatch::IntegrationTest
  test "should get demo" do
    get authors_demo_url
    assert_response :success
  end

  test "should get index" do
    get authors_index_url
    assert_response :success
  end

end
